FROM debian:buster
RUN apt update; apt install wget git  -y ; mkdir -p /etc/apt/keyrings; wget -O - https://packages.adoptium.net/artifactory/api/gpg/key/public | tee /etc/apt/keyrings/adoptium.asc ; echo "deb [signed-by=/etc/apt/keyrings/adoptium.asc] https://packages.adoptium.net/artifactory/deb $(awk -F= '/^VERSION_CODENAME/{print$2}' /etc/os-release) main" | tee /etc/apt/sources.list.d/adoptium.list ; apt update ; apt install temurin-8-jdk -y; mkdir /server ; cd /server ; wget 'https://maven.minecraftforge.net/net/minecraftforge/forge/1.7.10-10.13.4.1614-1.7.10/forge-1.7.10-10.13.4.1614-1.7.10-installer.jar' ; java -jar forge-1.7.10-10.13.4.1614-1.7.10-installer.jar --installServer
RUN cd /server ; git clone --depth=1 https://git.disroot.org/Cyber/cyber-modpack mods ; echo '#!/bin/bash\ncd /server ; java -Xmx900m -jar forge-1.7.10-10.13.4.1614-1.7.10-universal.jar nogui' > run.sh ; chmod +x run.sh
RUN cd /server ; echo 'eula=true' > eula.txt ; echo 'online-mode=false' > server.properties ; echo 'sed -i s/"forge-1.7.10-10.13.4.1614-1.7.10-universal.jar"/"minecraft_server.1.7.10.jar"/g run.sh no are more needed' 

CMD [ "/server/run.sh" ]
